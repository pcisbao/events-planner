FROM eclipse/ubuntu_jdk8:latest


ENV JAVA_APP_JAR api.jar
ENV JAVA_MAX_MEM_RATIO 40
ENV AB_JOLOKIA_OFF true

EXPOSE 8181

WORKDIR /deployments

COPY target/demo-0.0.1-SNAPSHOT.jar /deployments/app/api.jar

ENTRYPOINT java -jar /deployments/app/api.jar
