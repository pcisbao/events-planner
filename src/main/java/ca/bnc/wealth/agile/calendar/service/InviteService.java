package ca.bnc.wealth.agile.calendar.service;

import ca.bnc.wealth.agile.calendar.model.Event;
import ca.bnc.wealth.agile.calendar.model.Invite;
import ca.bnc.wealth.agile.calendar.repository.EventRepository;
import ca.bnc.wealth.agile.calendar.repository.InviteRepository;
import ca.bnc.wealth.agile.calendar.service.exception.UnknownEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;


@Service
@Transactional
public class InviteService {

    private final InviteRepository inviteRepository;
    private final EventRepository eventRepository;

    @Autowired
    public InviteService(InviteRepository inviteRepository, EventRepository eventRepository) {
        this.inviteRepository = inviteRepository;
        this.eventRepository = eventRepository;
    }

    public  Invite createInvite(String email, Long eventId) {
        Assert.notNull(email, "Email cannot be null");
        Assert.notNull(eventId, "EventId cannot be null");
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new UnknownEventException(eventId));
        final Invite newInvite = new Invite(email, event);
        return inviteRepository.save(newInvite);
    }
}
