package ca.bnc.wealth.agile.calendar.rest;

import ca.bnc.wealth.agile.calendar.model.Event;
import ca.bnc.wealth.agile.calendar.model.Invite;
import ca.bnc.wealth.agile.calendar.service.EventService;
import java.util.List;
import java.util.Optional;

import ca.bnc.wealth.agile.calendar.service.InviteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController()
@RequestMapping(value = "/events")
public class CalendarController {

  Logger log = LoggerFactory.getLogger(CalendarController.class);

  private final EventService eventService;
  private final InviteService inviteService;

  @Autowired
  public CalendarController(EventService eventService, InviteService inviteService) {
    this.eventService=eventService;
    this.inviteService=inviteService;
  }

  @GetMapping
  public List<Event> getAllEvents(){
    return eventService.getEvents();
  }

  @GetMapping(path="/{eventId}")
  public Event getEvent(@PathVariable Long eventId){
    return Optional.ofNullable(eventService.getEvent(eventId)).orElseThrow(()->new ResponseStatusException(
        HttpStatus.NOT_FOUND,"Not Found"));
  }

  @PostMapping
  public Event createEvent(@RequestBody Event event) {
    return eventService.createEvent(event);
  }

  @PutMapping(path="/{id}")
  public Event updateEvent(@PathVariable Long id, @RequestBody Event event) {
    event.setId(id);
    return eventService.updateEvent(event);
  }

  @DeleteMapping(path="/{id}")
  public void deleteEvent(@PathVariable Long id) {
      eventService.deleteEvent(id);
  }

  @PostMapping(path="/{id}/invite")
  public void addInvite(@RequestBody Invite invite, Long id) {
    inviteService.createInvite(invite.getEmail(), id);
  }

}
