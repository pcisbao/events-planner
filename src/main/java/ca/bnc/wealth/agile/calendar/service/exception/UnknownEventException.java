package ca.bnc.wealth.agile.calendar.service.exception;

public class UnknownEventException  extends RuntimeException {

    public UnknownEventException (Long id) {
        super("Unknown Event with ID=" + id);
    }

}
