package ca.bnc.wealth.agile.calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = {"ca.bnc.wealth.agile.calendar"})
@EnableSwagger2
public class AgileCalendarApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgileCalendarApplication.class, args);
	}

}
