package ca.bnc.wealth.agile.calendar.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import ca.bnc.wealth.agile.calendar.model.Event;
import ca.bnc.wealth.agile.calendar.service.EventService;
import ca.bnc.wealth.agile.calendar.service.InviteService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CalendarController.class}, initializers = {
    ConfigFileApplicationContextInitializer.class})
@ActiveProfiles({"test"})
@WebAppConfiguration
@EnableAutoConfiguration
public class CalendarControllerTest {

  @Autowired
  private WebApplicationContext context;

  private MockMvc mvc;

  @MockBean
  EventService eventService;

  @MockBean
  InviteService inviteService;

  ObjectMapper objectMapper;


  @Before
  public void setUp() {
    mvc = MockMvcBuilders
        .webAppContextSetup(context)
        .build();

    objectMapper = new ObjectMapper();

    Event event = new Event();
    event.setId(1L);
    event.setDescription("test");

    Mockito.when(eventService.getEvents()).thenReturn(Arrays.asList(event));

    Mockito.when(eventService.getEvent(1L)).thenReturn(event);

    Mockito.when(eventService.createEvent(event)).thenReturn(event);


  }

  @Test
  public void getAllEvents() throws Exception {

    MockHttpServletRequestBuilder builder =
        get("/events")
    .contentType(MediaType.APPLICATION_JSON_UTF8);
    String result = mvc.perform(builder).andExpect(status().isOk()).andReturn()
        .getResponse().getContentAsString();
    List events = objectMapper.readValue(result, new TypeReference<List<Event>>() {});
    assertNotNull(events);
    assertEquals(1,events.size());
    assertEquals(new Long(1), ((Event)events.get(0)).getId());

  }

  @Test
  public void getEvent() throws Exception {
    MockHttpServletRequestBuilder builder =
        get("/events/1")
            .contentType(MediaType.APPLICATION_JSON_UTF8);
    String result = mvc.perform(builder).andExpect(status().isOk()).andReturn()
        .getResponse().getContentAsString();
    Event event = objectMapper.readValue(result, Event.class);
    assertNotNull(event);
    //assertEquals(null, event.getId());
    assertEquals(new Long(1), event.getId());
  }


  @Test
  public void getEventNotFound() throws Exception {
    MockHttpServletRequestBuilder builder =
        get("/events/2")
            .contentType(MediaType.APPLICATION_JSON_UTF8);
    mvc.perform(builder).andExpect(status().is(404));
  }
}