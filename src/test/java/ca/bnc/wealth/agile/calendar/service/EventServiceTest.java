package ca.bnc.wealth.agile.calendar.service;

import ca.bnc.wealth.agile.calendar.model.Event;
import ca.bnc.wealth.agile.calendar.repository.EventRepository;
import ca.bnc.wealth.agile.calendar.rest.CalendarController;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {EventService.class}, initializers = {
        ConfigFileApplicationContextInitializer.class})
@ActiveProfiles({"test"})
public class EventServiceTest {

    @Autowired
    EventService eventService;

    @MockBean
    EventRepository eventRepository;

    @Before
    public void setup() {
        Event event = new Event(new Long(1), "mockTitle", "mockDescr", new Date(), new Date(), "mockLocation",null );
        when(eventRepository.findAll()).thenReturn(Arrays.asList(event));
        when(eventRepository.findById(1L)).thenReturn(java.util.Optional.of(event));

    }


    @Test
    public void getEvent() {
        Event event = eventService.getEvent(1L);
        Mockito.verify(eventRepository,times(1)).findById(1L);
    }

    @Test
    public void createEvent() {
        Event event = new Event(new Long(1), "mockTitle", "mockDescr", new Date(), new Date(), "mockLocation",null );
        eventService.createEvent(event);
        verify(eventRepository,times(1)).save(any());
    }

    @Test
    public void updateEvent() {
        Event event = new Event(new Long(1), "mockTitle", "mockDescr", new Date(), new Date(), "mockLocation",null );
        eventService.updateEvent(event);
        verify(eventRepository,times(1)).save(any());
    }

    @Test
    public void deleteEvent() {
        eventService.deleteEvent(1L);
        verify(eventRepository,times(1)).deleteById(1L);
    }

    @Test
    public void getEvents() {
        List<Event> events = eventService.getEvents();
        verify(eventRepository,times(1)).findAll();
    }

}